package com.productList.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PricesRequestDTO {

    protected Long brand;
    protected Long product;
    private String startDate;
    private String endDate;
    protected Integer priceList;
    protected Integer priority;
    protected Double price;
    protected String currency;

}
