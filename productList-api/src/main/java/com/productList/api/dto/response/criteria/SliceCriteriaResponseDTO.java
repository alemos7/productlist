package com.productList.api.dto.response.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SliceCriteriaResponseDTO implements Serializable {

    private Integer totalPages;
    private Integer pageSize;
    private Integer offset;
    private Integer totalRecords;

    List<?> results = new ArrayList<>();
}
