package com.productList.api.dto.response.criteria;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@ApiModel(
        value = "PricesCriteriaResponseDto",
        description = "Represents the data that identifies a prices."
)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PricesCriteriaResponseDTO implements Serializable {
    private Long brand;
    private Long product;
    private String startDate;
    private String endDate;
    private Integer priceList;
    private Integer priority;
    private Double price;
    private String currency;
    private Integer offset;
    private Integer pageSize;
}