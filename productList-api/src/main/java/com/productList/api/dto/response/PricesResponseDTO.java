package com.productList.api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PricesResponseDTO {

    protected BrandResponseDTO brand;
    protected ProductResponseDTO product;
    private String startDate;
    private String endDate;
    protected Integer priceList;
    protected Integer priority;
    protected Double price;
    protected String currency;

}
