package com.productList.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PricesSearchRequestDTO {

    protected Long brand;
    protected Long product;
    private String date;

}
