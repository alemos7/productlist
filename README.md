# Microservices - PricesList

El Microservicio  PricesList, es un microservicio que, como funcionalidad principal, nos proporciona la opción para poder conocer un precio de un producto dada un identificador de marca, un identificador de producto y una fecha para la cual se desea conocer su precio.

Es importante resaltar que para un producto podremos tener múltiples tarifas para diferentes fechas con un nivel de prioridad que es esta la que nos indicara cuál será el precio finalmente a ser proporcionado. El servicio para conocer dicha información es el que se encuentra dentro del directorio (Prices - getPriority )

Por otra parte, pero no menos relevante, lo invitamos a probar el resto de los servicios que disponemos, como por ejemplo, buscadores avanzados, listas generales de marcas y productos, entre otros.

Cada endpoint cuenta con ejemplos de respuestas Ok y KO.


------------
# Documentación
Planteamiento:
``` 
https://drive.google.com/file/d/1xMaZxJ8PM_x_pkN6iYFNvCHISZ-k1PfP/view?usp=sharing 
```


Postman:

``` 
https://documenter.getpostman.com/view/3612479/Uz5Gmb6x 
```
------------
# Ejecutar
``` 
1.- Clonar el repositorio
2.- Ejecutar proyecto 
3.- Ingresar a: http://localhost:8080/swagger-ui/
4.- Collection Postman: documentation/Inditex-pricesList.postman_collection.json
```