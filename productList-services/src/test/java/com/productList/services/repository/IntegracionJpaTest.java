package com.productList.services.repository;

import com.productList.services.repository.PricesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class IntegracionJpaTest {

    @Autowired
    PricesRepository pricesRepository;

    @Test
    void testPriceFindById() {
        var prices =  pricesRepository.findById(1L);
        assertTrue(prices.isPresent());
        assertEquals(35455, prices.orElseThrow().getProduct().getId());
    }

}
