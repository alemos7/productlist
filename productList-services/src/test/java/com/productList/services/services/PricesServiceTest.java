package com.productList.services.services;

import com.productList.api.dto.request.PricesSearchRequestDTO;
import com.productList.api.dto.response.PricesResponseDTO;
import com.productList.services.controller.utils.MockUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PricesServiceTest {

    @Mock
    PricesService pricesServices;

    @Test
    public void findByCriterial() throws IOException {

        PricesSearchRequestDTO pricesRequestDTO = MockUtils.getMock("dto/request/pricesRequestDTO.json", PricesSearchRequestDTO.class);
        PricesResponseDTO pricesResponseDTO = MockUtils.getMock("dto/response/pricesDTO.json", PricesResponseDTO.class);

        //Given

        when(pricesServices.findByBrandAndProductAndDate(pricesRequestDTO.getBrand(), pricesRequestDTO.getProduct(), pricesRequestDTO.getDate())).thenReturn(pricesResponseDTO);

        //When
        var result =  pricesServices.findByBrandAndProductAndDate(pricesRequestDTO.getBrand(), pricesRequestDTO.getProduct(), pricesRequestDTO.getDate());
        assertEquals((Double)75.5, result.getPrice());
    }
}