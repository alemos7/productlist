package com.productList.services.controller.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.io.CharStreams;

public class MockUtils {

    private static String fileEntity = "entity.json";
    private static String fileDto = "dto.json";

    /**
     *
     * @param path
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T getMock(String path, Class<T> clazz) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        ClassPathResource classPathResource = new ClassPathResource(path);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());

        return objectMapper.readValue(classPathResource.getInputStream(), clazz);
    }

    /**
     *
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T getMockEntity(Class<T> clazz) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        ClassPathResource classPathResource = new ClassPathResource(fileEntity);
        return objectMapper.readValue(classPathResource.getInputStream(), clazz);
    }

    /**
     *
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T getMockDto(Class<T> clazz) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        ClassPathResource classPathResource = new ClassPathResource(fileDto);
        return objectMapper.readValue(classPathResource.getInputStream(), clazz);
    }


    /**
     *
     * @return
     * @throws Exception
     */
    public static String getMockString(String fileDto) throws Exception {
        ClassPathResource classPathResource = new ClassPathResource(fileDto);
        InputStream inputStream = classPathResource.getInputStream();
        String content = null;
        try (final Reader reader = new InputStreamReader(inputStream)) {
            content = CharStreams.toString(reader);
        }
        return content;
    }
}
