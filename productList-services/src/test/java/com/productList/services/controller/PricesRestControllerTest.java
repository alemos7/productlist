package com.productList.services.controller;

import com.productList.api.dto.request.PricesSearchRequestDTO;
import com.productList.api.dto.response.PricesResponseDTO;
import com.productList.services.controller.utils.MockUtils;
import com.productList.services.entities.Prices;
import com.productList.services.services.PricesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(MockitoJUnitRunner.class)
@EnableWebMvc
public class PricesRestControllerTest {

    public static final String URL_ENDPOINT_TEST = "/api/v1/priceslist/search/priority/";
    @Autowired
    private MockMvc mvc;

    @Mock
    private PricesService pricesService;

    @InjectMocks
    private PricesRestController controller;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();

    }

    @Test
    public void getById() throws Exception {
        Prices prices = MockUtils.getMock("dto/prices.json", Prices.class);
        //Given
        when(pricesService.finById(1L)).thenReturn(Optional.of(prices).orElseThrow());
        //When
        mvc.perform(get("/api/v1/priceslist/1").contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.priceList").value(1))
                .andExpect(jsonPath("$.price").value(35.5D));
    }

    @Test
    public void testFindByBrandAndProductAndDate_t1() throws Exception {

        PricesSearchRequestDTO pricesRequestDTO = MockUtils.getMock("dto/request/searchPrices_test_1.json", PricesSearchRequestDTO.class);
        PricesResponseDTO pricesResponseDTO = MockUtils.getMock("dto/response/searchResponse_test_1.json", PricesResponseDTO.class);

        //Given
        when(pricesService.findByBrandAndProductAndDate(pricesRequestDTO.getBrand(), pricesRequestDTO.getProduct(), pricesRequestDTO.getDate())).thenReturn(pricesResponseDTO);

        //When
        mvc.perform(get(URL_ENDPOINT_TEST)
                        .param("brand", String.valueOf(pricesRequestDTO.getBrand()))
                        .param("product", String.valueOf(pricesRequestDTO.getProduct()))
                        .param("date", pricesRequestDTO.getDate())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.priceList").value(2))
                .andExpect(jsonPath("$.price").value(25.45D));

    }

    @Test
    public void testFindByBrandAndProductAndDate_t2() throws Exception {

        PricesSearchRequestDTO pricesRequestDTO = MockUtils.getMock("dto/request/searchPrices_test_2.json", PricesSearchRequestDTO.class);
        PricesResponseDTO pricesResponseDTO = MockUtils.getMock("dto/response/searchResponse_test_2.json", PricesResponseDTO.class);

        //Given
        when(pricesService.findByBrandAndProductAndDate(pricesRequestDTO.getBrand(), pricesRequestDTO.getProduct(), pricesRequestDTO.getDate())).thenReturn(pricesResponseDTO);

        //When
        mvc.perform(get(URL_ENDPOINT_TEST)
                        .param("brand", String.valueOf(pricesRequestDTO.getBrand()))
                        .param("product", String.valueOf(pricesRequestDTO.getProduct()))
                        .param("date", pricesRequestDTO.getDate())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.priceList").value(2))
                .andExpect(jsonPath("$.price").value(25.45D));
    }

    @Test
    public void testFindByBrandAndProductAndDate_t3() throws Exception {

        PricesSearchRequestDTO pricesRequestDTO = MockUtils.getMock("dto/request/searchPrices_test_3.json", PricesSearchRequestDTO.class);
        PricesResponseDTO pricesResponseDTO = MockUtils.getMock("dto/response/searchResponse_test_3.json", PricesResponseDTO.class);

        //Given
        when(pricesService.findByBrandAndProductAndDate(pricesRequestDTO.getBrand(), pricesRequestDTO.getProduct(), pricesRequestDTO.getDate())).thenReturn(pricesResponseDTO);

        //When
        mvc.perform(get(URL_ENDPOINT_TEST)
                        .param("brand", String.valueOf(pricesRequestDTO.getBrand()))
                        .param("product", String.valueOf(pricesRequestDTO.getProduct()))
                        .param("date", pricesRequestDTO.getDate())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.priceList").value(1))
                .andExpect(jsonPath("$.price").value(35.5D));
    }

    @Test
    public void testFindByBrandAndProductAndDate_t4() throws Exception {

        PricesSearchRequestDTO pricesRequestDTO = MockUtils.getMock("dto/request/searchPrices_test_4.json", PricesSearchRequestDTO.class);
        PricesResponseDTO pricesResponseDTO = MockUtils.getMock("dto/response/searchResponse_test_4.json", PricesResponseDTO.class);

        //Given
        when(pricesService.findByBrandAndProductAndDate(pricesRequestDTO.getBrand(), pricesRequestDTO.getProduct(), pricesRequestDTO.getDate())).thenReturn(pricesResponseDTO);

        //When
        mvc.perform(get(URL_ENDPOINT_TEST)
                        .param("brand", String.valueOf(pricesRequestDTO.getBrand()))
                        .param("product", String.valueOf(pricesRequestDTO.getProduct()))
                        .param("date", pricesRequestDTO.getDate())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.priceList").value(4))
                .andExpect(jsonPath("$.price").value(38.95D));
    }

    @Test
    public void testFindByBrandAndProductAndDate_t5() throws Exception {

        PricesSearchRequestDTO pricesRequestDTO = MockUtils.getMock("dto/request/searchPrices_test_5.json", PricesSearchRequestDTO.class);
        PricesResponseDTO pricesResponseDTO = MockUtils.getMock("dto/response/searchResponse_test_5.json", PricesResponseDTO.class);

        //Given
        when(pricesService.findByBrandAndProductAndDate(pricesRequestDTO.getBrand(), pricesRequestDTO.getProduct(), pricesRequestDTO.getDate())).thenReturn(pricesResponseDTO);

        //When
        mvc.perform(get(URL_ENDPOINT_TEST)
                        .param("brand", String.valueOf(pricesRequestDTO.getBrand()))
                        .param("product", String.valueOf(pricesRequestDTO.getProduct()))
                        .param("date", pricesRequestDTO.getDate())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.priceList").value(4))
                .andExpect(jsonPath("$.price").value(38.95D));
    }

}