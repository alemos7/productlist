package com.productList.services.repository.spec;

import com.productList.services.entities.Prices;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

public class PricesSpecifications {

	public static Specification<Prices> equalsBrand(Optional<Long> brandFilter) {
		return (root, query, builder) -> {
			return brandFilter.map( brand -> {
				return builder.equal(root.get("brand"),  brand);
			}).orElse(null);
		};
	}

	public static Specification<Prices> equalsProduct(Optional<Long> productFilter) {
		return (root, query, builder) -> {
			return productFilter.map( product -> {
				return builder.equal(root.get("product"),  product);
			}).orElse(null);
		};
	}

	public static Specification<Prices> equalsStartDate(Timestamp startDate) {
		return (root, query, builder) -> {
			if(!ObjectUtils.isEmpty(startDate)){
				return builder.greaterThanOrEqualTo(root.get("startDate"), startDate);
			}
			return null;
		};
	}

	public static Specification<Prices> equalsEndDate(Timestamp endDate) {
		return (root, query, builder) -> {
			if(!ObjectUtils.isEmpty(endDate)){
				return builder.greaterThanOrEqualTo(root.get("endDate"), endDate);
			}
			return null;
		};
	}

	public static Specification<Prices> equalsCurrency(Optional<String> currencyFilter) {
		return (root, query, builder) -> {
			return currencyFilter.map(currency -> {
				return builder.like(builder.upper(root.get("currency")), currency.toUpperCase() );
			}).orElse(null);
		};
	}

	public static Specification<Prices> equalsPricesList(Optional<Integer> priceListFilter) {
		return (root, query, builder) -> {
			return priceListFilter.map( priceList -> {
				return builder.equal(root.get("priceList"),  priceList);
			}).orElse(null);
		};
	}

	public static Specification<Prices> equalsPriority(Optional<Integer> priorityFilter) {
		return (root, query, builder) -> {
			return priorityFilter.map( priority -> {
				return builder.equal(root.get("priority"),  priority);
			}).orElse(null);
		};
	}

	public static Specification<Prices> equalsPrices(Optional<Double> priceFilter) {
		return (root, query, builder) -> {
			return priceFilter.map( price -> {
				return builder.equal(root.get("price"),  price);
			}).orElse(null);
		};
	}

}
