package com.productList.services.repository;

import com.productList.services.entities.Prices;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PricesRepository extends JpaRepository<Prices, Long> {

    @Query("SELECT p FROM Prices p WHERE p.brand.id = ?1 AND p.product.id = ?2  AND ?3  >= p.startDate AND ?3 <= p.endDate")
    List<Prices> findByBrandAndProductAndDate(Long brand, Long produc, Timestamp date);

    Page<Prices> findAll(Specification<Prices> and, Pageable pageable);

}