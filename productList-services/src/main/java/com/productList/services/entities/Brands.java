package com.productList.services.entities;

import lombok.Data;

import javax.persistence.*;

/**
 *
 * @author alemos
 */
@Entity
@Data
@Table(name = "BRANDS")
public class Brands {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Brands(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Brands() {

    }
}
