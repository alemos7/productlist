package com.productList.services.entities;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 *
 * @author alemos
 */

@Entity
@Data
@Table(name = "PRICES")
public class Prices {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "BRAND_ID", nullable = false)
    protected Brands brand;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", nullable = false)
    protected Products product;

    @Column(name = "START_DATE")
    private Timestamp startDate;

    @Column(name = "END_DATE")
    private Timestamp endDate;

    @Column(name = "PRICE_LIST")
    protected Integer priceList;

    @Column(name = "PRIORITY")
    protected Integer priority;

    @Column(name = "PRICE", precision=10, scale=2)
    protected Double price;

    @Column(name = "CURR")
    protected String currency;

    public Prices(Long id, Brands brand, Products product, Timestamp startDate, Timestamp endDate, Integer priceList, Integer priority, Double price, String currency) {
        this.id = id;
        this.brand = brand;
        this.product = product;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceList = priceList;
        this.priority = priority;
        this.price = price;
        this.currency = currency;
    }

    public Prices() {}
}
