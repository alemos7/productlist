package com.productList.services.entities;
import javax.persistence.*;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author alemos
 */
@Entity
@Data
@Table(name = "PRODUCTS")
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Products(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Products() {

    }
}
