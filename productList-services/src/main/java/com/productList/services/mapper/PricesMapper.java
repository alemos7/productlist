package com.productList.services.mapper;

import com.productList.api.dto.request.PricesRequestDTO;
import com.productList.api.dto.response.PricesResponseDTO;
import com.productList.services.entities.Prices;

public interface PricesMapper {
    PricesResponseDTO entityToDto(Prices entity);

    Prices dtoToEntity(PricesRequestDTO dto);
}
