package com.productList.services.mapper.impl;

import com.productList.api.dto.request.PricesRequestDTO;
import org.modelmapper.ModelMapper;
import com.productList.services.assets.exceptions.ErrorCatalogue;
import com.productList.services.assets.exceptions.GeneralException;
import com.productList.api.dto.response.PricesResponseDTO;
import com.productList.services.entities.Prices;
import com.productList.services.mapper.PricesMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Component
@AllArgsConstructor
@Slf4j
public class PricesMapperImpl implements PricesMapper {

    private final ModelMapper modelMapper = new ModelMapper();

    public PricesResponseDTO entityToDto(Prices entity){
        if (ObjectUtils.isEmpty(entity)) {
            throw new GeneralException(ErrorCatalogue.BAD_REQUEST, NOT_FOUND, ErrorCatalogue.BAD_REQUEST_DESC);
        }
        PricesResponseDTO dto = new PricesResponseDTO();
        modelMapper.map(entity, dto);
        return dto;
    }

    public Prices dtoToEntity(PricesRequestDTO dto){

        if (ObjectUtils.isEmpty(dto)) {
            throw new GeneralException(
                    ErrorCatalogue.BAD_REQUEST,
                    NOT_FOUND,
                    ErrorCatalogue.BAD_REQUEST_DESC);
        }
        Prices prices = new Prices();
        modelMapper.map(dto, prices);

        return prices;

    }

}
