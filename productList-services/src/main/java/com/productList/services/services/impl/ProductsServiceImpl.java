package com.productList.services.services.impl;

import com.productList.services.assets.exceptions.ErrorCatalogue;
import com.productList.services.assets.exceptions.GeneralException;
import com.productList.services.entities.Brands;
import com.productList.services.entities.Products;
import com.productList.services.repository.ProductsRepository;
import com.productList.services.services.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class ProductsServiceImpl implements ProductsService {

    @Autowired
    private ProductsRepository productsRepository;

    @Override
    public Products finById(Long id) {
        return productsRepository.findById(id).orElseThrow(() -> new GeneralException(ErrorCatalogue.PRODUCT_NOT_EXIST,BAD_REQUEST,ErrorCatalogue.PRODUCT_NOT_EXIST_DESC));
    }

    @Override
    public List<Products> finAll() {
        return productsRepository.findAll();
    }

}
