package com.productList.services.services.impl;

import com.productList.services.assets.exceptions.ErrorCatalogue;
import com.productList.services.assets.exceptions.GeneralException;
import com.productList.services.entities.Brands;
import com.productList.services.entities.Prices;
import com.productList.services.repository.BrandsRepository;
import com.productList.services.services.BrandsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class BrandsServiceImpl implements BrandsService {

    @Autowired
    private BrandsRepository brandsRepository;

    @Override
    public Brands finById(Long id) {
        return brandsRepository.findById(id).orElseThrow(() -> new GeneralException(ErrorCatalogue.BRAND_NOT_EXIST,BAD_REQUEST,ErrorCatalogue.BRAND_NOT_EXIST_DESC));
    }
    @Override
    public List<Brands> finAll() {
        return brandsRepository.findAll();
    }
}
