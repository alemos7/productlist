package com.productList.services.services.impl;

import com.google.common.collect.Iterables;
import com.productList.api.dto.response.criteria.PricesCriteriaResponseDTO;
import com.productList.api.dto.response.criteria.SliceCriteriaResponseDTO;
import com.productList.services.assets.exceptions.ErrorCatalogue;
import com.productList.services.assets.exceptions.GeneralException;
import com.productList.api.dto.request.PricesRequestDTO;
import com.productList.api.dto.request.PricesSearchRequestDTO;
import com.productList.api.dto.response.PricesResponseDTO;
import com.productList.services.assets.utils.SliceUtils;
import com.productList.services.entities.Brands;
import com.productList.services.entities.Prices;
import com.productList.services.entities.Products;
import com.productList.services.mapper.PricesMapper;
import com.productList.services.repository.PricesRepository;
import com.productList.services.repository.spec.PricesSpecifications;
import com.productList.services.services.BrandsService;
import com.productList.services.services.PricesService;
import com.productList.services.services.ProductsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@Slf4j
@AllArgsConstructor
public class PricesServiceImpl implements PricesService {

    private final PricesRepository pricesRepository;

    private final PricesMapper mapper;

    private final ProductsService productsService;

    private final BrandsService brandsService;


    @Transactional
    @Override
    public PricesResponseDTO savePrices(PricesRequestDTO dto){
        PricesResponseDTO savedDto=null;
        log.debug("Entering savePrices [dto]: {}",dto);

        Prices entity = mapper.dtoToEntity(dto);

        Products product = productsService.finById(dto.getProduct());
        Brands brand = brandsService.finById(dto.getBrand());

        entity.setStartDate(convertStringToTimestamp(dto.getStartDate()));
        entity.setEndDate(convertStringToTimestamp(dto.getEndDate()));
        entity.setProduct(product);
        entity.setBrand(brand);

        Prices saved = pricesRepository.save(entity);
        savedDto = mapper.entityToDto(saved);

        log.debug("Leaving saveQualifier [savedDto]: {}",savedDto);
        return savedDto;
    }

    @Transactional(readOnly = true)
    @Override
    public PricesResponseDTO findByBrandAndProductAndDate(Long brand, Long product, String date){
        //log.debug("Entering findByBrandAndProductAndDate [uuid]: {}", dto);
        List<Prices> price = pricesRepository.findByBrandAndProductAndDate(brand, product, convertStringToTimestamp(date));
        if(price.size()==0) throw new GeneralException(ErrorCatalogue.DOES_NOT_EXIST,NOT_FOUND,ErrorCatalogue.DOES_NOT_EXIST_DESC);
        PricesResponseDTO priceDTO = mapper.entityToDto(Iterables.getLast(price));
        return priceDTO;
    }

    public Timestamp convertStringToTimestamp(String strDate) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parsedDate = dateFormat.parse(strDate);
            Timestamp timeStampDate = new Timestamp(parsedDate.getTime());
            return timeStampDate;
        } catch (ParseException e) {
            throw new GeneralException(ErrorCatalogue.BAD_REQUEST,BAD_REQUEST,ErrorCatalogue.BAD_REQUEST_DESC);
        }
    }

    @Override
    public Prices finById(Long id) {
        if(!existsById(id))throw new GeneralException(ErrorCatalogue.DOES_NOT_EXIST,NOT_FOUND,ErrorCatalogue.DOES_NOT_EXIST_DESC);
        return pricesRepository.findById(id).orElseThrow();
    }

    @Override
    public Boolean existsById(Long id) {
        return pricesRepository.existsById(id);
    }

    @Override
    public SliceCriteriaResponseDTO findPricesByCriteria(PricesCriteriaResponseDTO filters){
        Pageable pageable = PageRequest.of(filters.getOffset(), filters.getPageSize(), Sort.by(Sort.Direction.DESC, "id"));

        Timestamp startDate = (filters.getStartDate()!=null)?convertStringToTimestamp(filters.getStartDate()):null;
        Timestamp endDate = (filters.getEndDate()!=null)?convertStringToTimestamp(filters.getEndDate()):null;

        Page<Prices> page = pricesRepository.findAll(
                PricesSpecifications.equalsBrand(Optional.ofNullable(filters.getBrand()))
                        .and(PricesSpecifications.equalsProduct(Optional.ofNullable(filters.getProduct())))
                        .and(PricesSpecifications.equalsStartDate(startDate))
                        .and(PricesSpecifications.equalsEndDate(endDate))
                        .and(PricesSpecifications.equalsPricesList(Optional.ofNullable(filters.getPriceList())))
                        .and(PricesSpecifications.equalsPriority(Optional.ofNullable(filters.getPriority())))
                        .and(PricesSpecifications.equalsCurrency(Optional.ofNullable(filters.getCurrency())))
                , pageable);

        List<Prices> contractProductResponse = (List<Prices>)
                SliceUtils.sliceCriteriaResponse(filters.getOffset(), filters.getPageSize(), page).getResults();

        SliceCriteriaResponseDTO response = SliceUtils.sliceCriteriaResponse(filters.getOffset(), filters.getPageSize(), page);
        response.setResults(contractProductResponse.stream().map(mapper::entityToDto).collect(Collectors.toList()));

        return response;
    }

    @Override
    public List<Prices> finAll() {
        return pricesRepository.findAll();
    }
}
