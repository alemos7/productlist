package com.productList.services.services;

import com.productList.services.entities.Brands;
import com.productList.services.entities.Products;

import java.util.List;

public interface ProductsService {

    Products finById(Long id);

    List<Products> finAll();
}
