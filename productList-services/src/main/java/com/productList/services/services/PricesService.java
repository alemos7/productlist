package com.productList.services.services;

import com.productList.api.dto.request.PricesRequestDTO;
import com.productList.api.dto.request.PricesSearchRequestDTO;
import com.productList.api.dto.response.PricesResponseDTO;
import com.productList.api.dto.response.criteria.PricesCriteriaResponseDTO;
import com.productList.api.dto.response.criteria.SliceCriteriaResponseDTO;
import com.productList.services.entities.Prices;

import java.util.List;

public interface PricesService {

    PricesResponseDTO savePrices(PricesRequestDTO dto);

    PricesResponseDTO findByBrandAndProductAndDate(Long brand, Long product, String date);

    Prices finById(Long id);

    Boolean existsById(Long id);

    SliceCriteriaResponseDTO findPricesByCriteria(PricesCriteriaResponseDTO filters);

    List<Prices> finAll();

}
