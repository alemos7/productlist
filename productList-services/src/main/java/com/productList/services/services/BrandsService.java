package com.productList.services.services;

import com.productList.services.entities.Brands;
import com.productList.services.entities.Prices;

import java.util.List;

public interface BrandsService {

    List<Brands> finAll();
    Brands finById(Long id);

}
