package com.productList.services.controller;

import com.productList.services.assets.annotations.ResponseTime;
import com.productList.services.entities.Brands;
import com.productList.services.services.BrandsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author alemos
 */
@RestController
@RequestMapping("/api/v1/brand")
public class BrandsRestController {

    @Autowired
    private BrandsService brandsService;

    @ResponseTime
    @GetMapping()
    @CacheEvict(value = "brand", allEntries = true)
    public List<Brands> getAll() {
        return brandsService.finAll();
    }

    @ResponseTime
    @GetMapping("/{id}")
    @CacheEvict(value = "brand", allEntries = true)
    public Brands getById(@PathVariable Long id) {
        return brandsService.finById(id);
    }

}
