package com.productList.services.controller;

import javax.validation.Valid;

import com.productList.api.dto.response.criteria.PricesCriteriaResponseDTO;
import com.productList.api.dto.response.criteria.SliceCriteriaResponseDTO;
import com.productList.services.assets.annotations.ResponseTime;
import com.productList.api.dto.request.PricesRequestDTO;
import com.productList.services.entities.Prices;
import com.productList.services.services.PricesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.productList.api.dto.response.PricesResponseDTO;
import java.util.List;
import static org.springframework.http.HttpStatus.*;

/**
 *
 * @author alemos
 */

@RestController
@Api("PricesRestController")
@RequestMapping("/api/v1/priceslist")
public class PricesRestController {

    @Autowired
    private PricesService pricesService;

    @ResponseTime
    @GetMapping()
    @CacheEvict(value = "priceslist", allEntries = true)
    public List<Prices> getAll() {
        return pricesService.finAll();
    }

    @ResponseTime
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    @CacheEvict(value = "priceslist", allEntries = true)
    public Prices getById(@PathVariable Long id) {
        return pricesService.finById(id);
    }

    @ResponseTime
    @PostMapping
    @ResponseStatus(ACCEPTED)
    public ResponseEntity<PricesResponseDTO> post(@Valid @RequestBody PricesRequestDTO request) {
        PricesResponseDTO savedDto = pricesService.savePrices(request);
        return ResponseEntity.noContent().header("Location", String.valueOf(savedDto.getPrice())).build();
    }

    @ResponseTime
    @GetMapping("/search/priority/")
    @ResponseStatus(OK)
    @CacheEvict(value = "priceslist", allEntries = true)
    public PricesResponseDTO findByBrandAndProductAndDate(
            @RequestParam(required = true, name = "brand") Long brand,
            @RequestParam(required = true, name = "product") Long product,
            @RequestParam(required = true, name = "date") String date
    ) {
        PricesResponseDTO price = pricesService.findByBrandAndProductAndDate(brand, product, date);
        return price;
    }

    @ResponseTime
    @GetMapping(value = "/search/criteria")
    @ResponseStatus(OK)
    @CacheEvict(value = "priceslist", allEntries = true)
    public ResponseEntity<SliceCriteriaResponseDTO> findPricesByCriteria(
            @ApiParam(name = "brand", value = "The code Branch", example = "1") @RequestParam(required = false, name = "brand") Long brand,
            @ApiParam(name = "product", value = "The code Product", example = "35455") @RequestParam(required = false, name = "product") Long product,
            @ApiParam(name = "startDate", value = "The Start Date", example = "2020-06-14 10:00:00") @RequestParam(required = false, name = "startDate") String startDate,
            @ApiParam(name = "endDate", value = "The End Date", example = "2020-06-15 10:00:00") @RequestParam(required = false, name = "endDate") String endDate,
            @ApiParam(name = "priceList", value = "The Price List", example = "1") @RequestParam(required = false, name = "priceList") Integer priceList,
            @ApiParam(name = "priority", value = "The Priority", example = "1") @RequestParam(required = false, name = "priority") Integer priority,
            @ApiParam(name = "price", value = "Price", example = "35.35") @RequestParam(required = false, name = "price") Double price,
            @ApiParam(name = "currency", value = "Currency", example = "EUR") @RequestParam(required = false, name = "currency") String currency,
            @ApiParam(name="pageSize", value = "Number of elements to be returned", example = "10")
            @RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize,
            @ApiParam(name="offset", value = "Index from which the result must be fetched", example = "0")
            @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset) {

        PricesCriteriaResponseDTO criteria = PricesCriteriaResponseDTO
                .builder()
                .brand(brand)
                .product(product)
                .startDate(startDate)
                .endDate(endDate)
                .priceList(priceList)
                .priority(priority)
                .price(price)
                .currency(currency)
                .offset(offset)
                .pageSize(pageSize)
                .build();

        SliceCriteriaResponseDTO response = pricesService.findPricesByCriteria(criteria);
        return ResponseEntity.ok().body(response);
    }
}
