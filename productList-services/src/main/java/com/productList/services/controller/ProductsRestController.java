package com.productList.services.controller;

import com.productList.services.assets.annotations.ResponseTime;
import com.productList.services.entities.Products;
import com.productList.services.services.ProductsService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author alemos
 */
@RestController
@RequestMapping("/api/v1/product")
public class ProductsRestController {

    @Autowired
    private ProductsService productsService;

    @GetMapping()
    @ResponseTime
    @CacheEvict(value = "brand", allEntries = true)
    public List<Products> getAll() {
        return productsService.finAll();
    }

    @GetMapping("/{id}")
    @ResponseTime
    @CacheEvict(value = "brand", allEntries = true)
    public Products getById(@PathVariable Long id) {
        return productsService.finById(id);
    }

}
