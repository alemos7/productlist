package com.productList.services.controller;

import com.productList.services.assets.exceptions.GeneralException;
import com.productList.services.assets.exceptions.dto.ErrorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(value = GeneralException.class)
    public ResponseEntity<ErrorDTO> requestExceptionHandler(GeneralException exception){
        ErrorDTO error = ErrorDTO.builder().code(exception.getCode()).message(exception.getMessage()).build();
        return new ResponseEntity<>(error, exception.getStatus());
    }
}
