package com.productList.services.assets.annotations.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class ManagementAnnotations {

    private static Logger log = LoggerFactory.getLogger(ManagementAnnotations.class);

    @Around(value = "@annotation(com.productList.services.assets.annotations.ResponseTime)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long timeTaken = System.currentTimeMillis() - startTime;
        log.info("Time Taken by {} is {} miliseconds", joinPoint.getSignature().getName(), timeTaken);
        return result;
    }

}