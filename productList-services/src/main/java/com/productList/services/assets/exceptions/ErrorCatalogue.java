package com.productList.services.assets.exceptions;

public class ErrorCatalogue {
    public static final String PRODUCT_NOT_EXIST = "1000";
    public static final String PRODUCT_NOT_EXIST_DESC = "The indicated product does not exist";

    public static final String BRAND_NOT_EXIST = "2000";
    public static final String BRAND_NOT_EXIST_DESC = "The indicated brand does not exist";

    public static final String BAD_REQUEST = "400";
    public static final String BAD_REQUEST_DESC = "Error in the input parameters";

    public static final String DOES_NOT_EXIST = "404";
    public static final String DOES_NOT_EXIST_DESC = "The indicated record does not exist";
}
