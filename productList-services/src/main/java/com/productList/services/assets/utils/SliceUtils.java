package com.productList.services.assets.utils;

import com.productList.api.dto.response.criteria.SliceCriteriaResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.util.ObjectUtils;

import java.util.List;

public class SliceUtils {

    /**
     * method used to return any paged list of objects (used in type query)
     *
     * @param offset
     * @param pageSize
     * @param results
     * @return SliceCriteriaResponseDTO
     */
    public static SliceCriteriaResponseDTO sliceCriteriaResponse(Integer offset, Integer pageSize, Integer totalResults, List<?> results){
        SliceCriteriaResponseDTO response = new SliceCriteriaResponseDTO();

        Integer totalPageCount = Double.valueOf(Math.ceil((double)results.size() / pageSize)).intValue();

        response.setOffset(offset);
        response.setPageSize(pageSize);
        response.setTotalPages(totalPageCount);
        response.setTotalRecords(totalResults);
        response.setResults(results);

        return response;
    }

    /**
     * method used to return any paged list of objects (used in jpa specification)
     *
     * @param offset
     * @param pageSize
     * @param page
     * @return SliceCriteriaResponseDTO
     */
    public static SliceCriteriaResponseDTO sliceCriteriaResponse(Integer offset, Integer pageSize, Page page){
        SliceCriteriaResponseDTO response = new SliceCriteriaResponseDTO();

        if(!ObjectUtils.isEmpty(page)){
            response.setOffset(offset);
            response.setPageSize(pageSize);
            response.setTotalPages(page.getTotalPages());
            response.setTotalRecords((int) page.getTotalElements());
            response.setResults(page.toList());
        }

        return response;
    }
}
